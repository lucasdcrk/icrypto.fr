@extends('layouts.frontend')

@section('title', 'Accueil')

@section('content')
<div class="section">
  <div class="container">
    <div class="row">
      <div class="col s12 l3 offset-l3">
        <h1 class="header center blue-grey-text brand-name">iCrypto.fr</h1>
      </div>
      <div class="col l4 hide-on-med-and-down">
        <img height="150px" src="{{ url('assets/img/21747-full.png') }}">
      </div>
    </div>
  </div>
</div>

<div class="section">
  <div class="container">
    <div class="row">
      <div class="col s12 l8">
        <h3>Bienvenue sur le site iCrypto !</h3>
        <p>
          Sur ce site, vous aurez accès à tout ce qui est nécessaire pour plus se sentir trader.<br>
          Nous proposons donc une <a href="{{ url('tools/calculator') }}">calculatrice</a> avec de multiples fonctions qui vous permettront de calculer vos bénéfices ou à quel niveau placer un ordre pour faire un certain bénéfice.<br>
          J’espère que vous vous plairez ici et que le site vous offrira la meilleur expérience possible.
        </p>
        <h5>Bon trading !</h5>
      </div>
      <div class="col s12 l4">
        <img src="{{ url('assets/img/1503518936-risitas-economiste.png') }}">
      </div>
    </div>
  </div>
</div>
@endsection
