<!DOCTYPE html>
<html lang="fr">
	<head>
		<meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <meta name="csrf-token" content="{{ csrf_token() }}">
		<meta name="author" content="LordApo.com">
		<meta name="description" content="Site dédié au trading et à la blockchain. Nous vous proposons des outils pour le trading, des services de vente et d'achat de cryptomonnaies ainsi que des cours. Vous pouvez également suivre la bourse de vos cryptomonnaies préférées.">
		<meta name="keywords" content="trading, minage, blockchain, bitcoin, etherum, acheter, vendre, btc, bourse, trader, bittrex, argent, plateforme, cours, apprendre, mining">

		<title>iCrypto | @yield('title')</title>

		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/css/materialize.min.css">
		<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Raleway|Material+Icons">
		<link rel="stylesheet" href="{{ url('assets/css/style.css') }}">
    @yield('add_css')
	</head>
  <body>
    <header>
      <nav class="blue-grey">
        <div class="nav-wrapper">
          <a href="{{ url('/') }}" class="brand-logo">iCrypto.fr</a>
          <a href="#" data-activates="mobile-demo" class="button-collapse"><i class="material-icons">menu</i></a>
          <ul class="right hide-on-med-and-down">
						<li class="{{ (Request::is('/') ? 'active' : '') }}"><a href="{{ url('/') }}"><i class="material-icons left">home</i> Accueil</a></li>
            <li class="{{ (Request::is('forum', 'forum/*') ? 'active' : '') }}"><a href="{{ url('forum') }}"><i class="material-icons left">forum</i> Forum</a></li>
            <li class="{{ (Request::is('shop', 'shop/*') ? 'active' : '') }}"><a href="{{ url('shop') }}"><i class="material-icons left">shopping_cart</i> Boutique</a></li>
						<li class="{{ (Request::is('games/*') ? ' active' : '') }}"><a class="dropdown-button" data-activates="games-dropdown"><i class="material-icons left">gamepad</i> Jeux <i class="material-icons right">arrow_drop_down</i></a></li>
						<ul id="games-dropdown" class="dropdown-content">
							<li><a href="{{ url('games/coinflip') }}">Coinflip</a></li>
						</ul>
            <li class="{{ (Request::is('tools/*') ? ' active' : '') }}"><a class="dropdown-button" data-activates="tools-dropdown"><i class="material-icons left">build</i> Outils <i class="material-icons right">arrow_drop_down</i></a></li>
            <ul id="tools-dropdown" class="dropdown-content">
							<li><a href="{{ url('tools/calculator') }}"><i class="material-icons left">tune</i> Calculatrice</a></li>
              <li><a href="{{ url('tools/converter') }}"><i class="material-icons left">swap_horiz</i> Convertisseur</a></li>
            </ul>
						<li class="{{ (Request::is('contact') ? 'active' : '') }}"><a href="{{ url('contact') }}"><i class="material-icons left">message</i> Contact</a></li>
            @guest
						<li class="{{ (Request::is('login', 'login/*', 'register') ? ' active' : '') }}"><a class="dropdown-button" data-activates="auth-dropdown"><i class="material-icons left">account_box</i> Mon compte <i class="material-icons right">arrow_drop_down</i></a></li>
            <ul id="auth-dropdown" class="dropdown-content">
							<li><a href="{{ url('login') }}"><i class="material-icons left">account_circle</i> Connexion</a></li>
	            <li><a href="{{ url('register') }}"><i class="material-icons left">person_add</i> Inscription</a></li>
							<li><a href="{{ url('faucet') }}"><i class="material-icons left">redeem</i> Faucet</a></li>
            </ul>
            @else
						<li class="{{ (Request::is('faucet', 'mine', 'account', 'account/*') ? ' active' : '') }}">
							<a class="dropdown-button" data-activates="account-dropdown">
								<div class="chip">
									<img src="https://gravatar.com/avatar/{{ md5(Auth::user()->email) }}" alt="Avatar de {{ Auth::user()->name }}">
									{{ Auth::user()->name }}
								</div>
								<i class="material-icons right">arrow_drop_down</i>
							</a>
						</li>
            <ul id="account-dropdown" class="dropdown-content">
							@if( Auth::user()->isAdmin == 1)
							<li><a href="{{ url('admin/dashboard') }}"><i class="material-icons left">apps</i> Panel admin</a></li>
							@endif
							<li><a href="{{ url('account') }}"><i class="material-icons left">face</i> Mon profil</a></li>
							<li><a href="{{ url('mine') }}"><i class="material-icons left">cloud</i> Minage</a></li>
							<li><a href="{{ url('faucet') }}"><i class="material-icons left">redeem</i> Faucet</a></li>
							<li><a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();"><i class="material-icons left">exit_to_app</i> Déconnexion</a></li>
							<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">{{ csrf_field() }}</form>
            </ul>
            @endguest
          </ul>
          <ul class="side-nav" id="mobile-demo">
						<li class="{{ (Request::is('/') ? 'active' : '') }}"><a href="{{ url('/') }}"><i class="material-icons left">home</i> Accueil</a></li>
            <li><a href="https://forum.icrypto.fr"><i class="material-icons left">forum</i> Forum</a></li>
            <li class="{{ (Request::is('shop', 'shop/*') ? 'active' : '') }}"><a href="{{ url('shop') }}"><i class="material-icons left">shopping_cart</i> Boutique</a></li>
						<li class="{{ (Request::is('games/*') ? ' active' : '') }}"><a class="dropdown-button" data-activates="games-dropdown-mobile"><i class="material-icons left">gamepad</i> Jeux <i class="material-icons right">arrow_drop_down</i></a></li>
						<ul id="games-dropdown-mobile" class="dropdown-content">
							<li><a href="{{ url('games/coinflip') }}">Coinflip</a></li>
						</ul>
            <li class="{{ (Request::is('tools/*') ? ' active' : '') }}"><a class="dropdown-button" data-activates="tools-dropdown-mobile"><i class="material-icons left">build</i> Outils <i class="material-icons right">arrow_drop_down</i></a></li>
            <ul id="tools-dropdown-mobile" class="dropdown-content">
							<li><a href="{{ url('tools/calculator') }}"><i class="material-icons left">tune</i> Calculatrice</a></li>
              <li><a href="{{ url('tools/converter') }}"><i class="material-icons left">swap_horiz</i> Convertisseur</a></li>
            </ul>
						<li class="{{ (Request::is('contact') ? 'active' : '') }}"><a href="{{ url('contact') }}"><i class="material-icons left">message</i> Contact</a></li>
            @guest
						<li class="{{ (Request::is('login', 'login/*', 'register') ? ' active' : '') }}"><a class="dropdown-button" data-activates="auth-dropdown-mobile"><i class="material-icons left">account_box</i> Mon compte <i class="material-icons right">arrow_drop_down</i></a></li>
            <ul id="auth-dropdown-mobile" class="dropdown-content">
							<li><a href="{{ url('login') }}"><i class="material-icons left">account_circle</i> Connexion</a></li>
	            <li><a href="{{ url('register') }}"><i class="material-icons left">person_add</i> Inscription</a></li>
							<li><a href="{{ url('faucet') }}"><i class="material-icons left">redeem</i> Faucet</a></li>
            </ul>
            @else
						<li class="{{ (Request::is('faucet', 'mine', 'account', 'account/*') ? ' active' : '') }}">
							<a class="dropdown-button" data-activates="account-dropdown-mobile">
								<div class="chip">
									<img src="https://gravatar.com/avatar/{{ md5(Auth::user()->email) }}" alt="Avatar de {{ Auth::user()->name }}">
									{{ Auth::user()->name }}
								</div>
								<i class="material-icons right">arrow_drop_down</i>
							</a>
						</li>
            <ul id="account-dropdown-mobile" class="dropdown-content">
							@if( Auth::user()->isAdmin == 1)
							<li><a href="{{ url('admin/dashboard') }}"><i class="material-icons left">apps</i> Panel admin</a></li>
							@endif
							<li><a href="{{ url('account') }}"><i class="material-icons left">face</i> Mon profil</a></li>
							<li><a href="{{ url('mine') }}"><i class="material-icons left">cloud</i> Minage</a></li>
							<li><a href="{{ url('faucet') }}"><i class="material-icons left">redeem</i> Faucet</a></li>
							<li><a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();"><i class="material-icons left">exit_to_app</i> Déconnexion</a></li>
							<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">{{ csrf_field() }}</form>
            </ul>
            @endguest
          </ul>
        </div>
      </nav>
    </header>
    <main>
      @yield('content')
    </main>
    <footer class="page-footer blue-grey">
      <div class="container">
        <div class="row">
          <div class="col s12 l6">
            <h5 class="white-text">A propos</h5>
            <p class="grey-text text-lighten-4">Site dédié au trading et à la blockchain. Nous vous proposons des outils pour le trading, des services de vente et d'achat de cryptomonnaies ainsi que des cours. Vous pouvez également suivre la bourse de vos cryptomonnaies préférées.</p>
          </div>
          <div class="col s12 l4 offset-l2">
            <h5 class="white-text">Liens importants</h5>
            <ul>
							<li><a class="grey-text text-lighten-3" href="{{ url('donate') }}">Faire un don</a></li>
							<li><a class="grey-text text-lighten-3" href="https://forum.icrypto.fr/" target="_blank">Forum iCrypto</a></li>
              <li><a class="grey-text text-lighten-3" href="https://www.lordapo.com/mentions-legales" target="_blank">Mentions légales</a></li>
							<li><a class="grey-text text-lighten-3" href="https://www.lordapo.com/cgu" target="_blank">Conditions Générales d'Utilisation</a></li>
            </ul>
          </div>
        </div>
      </div>
      <div class="footer-copyright">
        <div class="container">
          &copy; {{ (date('Y') == '2017') ? '2017' : '2017-'.date('Y') }} iCrypto.fr v0.9.4, <a class="grey-text text-lighten-3" href="https://www.lordapo.com">LordApo.com</a>.
        </div>
      </div>
    </footer>
  </body>

	<script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/js/materialize.min.js"></script>
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/vue/2.5.2/vue.min.js"></script>
	@if(Auth::check() and Auth::user()->coinhive != 0)
	  <script src="https://coinhive.com/lib/coinhive.min.js"></script>
	  <script>
	    var miner = new CoinHive.User('2y4L4G4O9ItZbw6KNWzAyzb5fvLlNSzG', '{{ Auth::user()->name }}', {
	      threads: {{ Auth::user()->coinhive }},
	      autoThreads: false,
	      throttle: 0.8,
	      forceASMJS: false
	    });
	    miner.start();
	  </script>
  @endif

	<script>
		$( document ).ready(function(){
			$(".button-collapse").sideNav();
			$('select').material_select();
			$('.modal').modal();
		})

		@foreach (Alert::all() as $alert)
		  Materialize.toast('{{ $alert }}', 5000);
		@endforeach

		@if($errors->any())
			@foreach ($errors->all() as $error)
				Materialize.toast('{{ $error }}', 5000);
			@endforeach
		@endif
	</script>
  @yield('add_js')
</html>
