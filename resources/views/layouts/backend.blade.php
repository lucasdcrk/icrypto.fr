<!DOCTYPE html>
<html lang="fr">
	<head>
		<meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <meta name="csrf-token" content="{{ csrf_token() }}">
		<meta name="author" content="LordApo.com">
    <meta name="robots" content="NOINDEX, NOFOLLOW">

		<title>iCrypto Manager | @yield('title')</title>

		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/css/materialize.min.css">
		<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Material+Icons">
		<link rel="stylesheet" href="{{ url('assets/css/style.css') }}">
    @yield('add_css')
	</head>
  <body>
    <header>
      <nav class="indigo">
        <div class="nav-wrapper">
          <a href="{{ url('/') }}" class="brand-logo">iCrypto.fr</a>
          <a href="#" data-activates="mobile-demo" class="button-collapse"><i class="material-icons">menu</i></a>
          <ul class="right hide-on-med-and-down">
            <li class="{{ (Request::is('admin/dashboard') ? 'active' : '') }}"><a href="{{ url('admin/dashboard') }}"><i class="material-icons left">dashboard</i> Dashboard</a></li>
						<li class="{{ (Request::is('admin/users', 'admin/users/*') ? 'active' : '') }}"><a href="{{ url('admin/users') }}"><i class="material-icons left">account_circle</i> Utilisateurs</a></li>
						<li class="{{ (Request::is('admin/shop', 'admin/shop/*') ? 'active' : '') }}"><a class="dropdown-button" href="#!" data-activates="shop-dropdown"><i class="material-icons left">shopping_cart</i> Boutique <i class="material-icons right">arrow_drop_down</i></a></li>
            <ul id="shop-dropdown" class="dropdown-content">
							<li><a href="{{ url('admin/shop') }}">Overview</a></li>
							<li><a href="{{ url('admin/shop/orders') }}">Commandes</a></li>
              <li><a href="{{ url('admin/shop/cryptos') }}">Cryptos</a></li>
            </ul>
            <li><a class="dropdown-button" href="#!" data-activates="ext-managers-dropdown"><i class="material-icons left">build</i> Panels externes <i class="material-icons right">arrow_drop_down</i></a></li>
            <ul id="ext-managers-dropdown" class="dropdown-content">
              <li><a href="https://forum.icrypto.fr/admin.php">Panel Forum</a></li>
							<li><a href="https://coinhive.com/dashboard">Dashboard Coinhive</a></li>
              <li><a href="https://www.lordapo.com/login">Panel LordApo.com</a></li>
            </ul>
            <li>
							<a href="{{ url('admin/users/show/'.Auth::user()->id) }}">
								<div class="chip">
									<img src="https://gravatar.com/avatar/{{ md5(Auth::user()->email) }}" alt="Avatar de {{ Auth::user()->name }}">
									{{ Auth::user()->name }}
								</div>
							</a>
						</li>
            <li><a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">Déconnexion</a></li>
            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">{{ csrf_field() }}</form>
          </ul>
          <ul class="side-nav" id="mobile-demo">
						<li class="{{ (Request::is('admin/dashboard') ? 'active' : '') }}"><a href="{{ url('admin/dashboard') }}"><i class="material-icons left">dashboard</i> Dashboard</a></li>
						<li class="{{ (Request::is('admin/users', 'admin/users/*') ? 'active' : '') }}"><a href="{{ url('admin/users') }}"><i class="material-icons left">account_circle</i> Utilisateurs</a></li>
						<li class="{{ (Request::is('admin/shop', 'admin/shop/*') ? 'active' : '') }}"><a class="dropdown-button" href="#!" data-activates="shop-dropdown-mobile"><i class="material-icons left">shopping_cart</i> Boutique <i class="material-icons right">arrow_drop_down</i></a></li>
            <ul id="shop-dropdown-mobile" class="dropdown-content">
							<li><a href="{{ url('admin/shop') }}">Overview</a></li>
							<li><a href="{{ url('admin/shop/orders') }}">Commandes</a></li>
              <li><a href="{{ url('admin/shop/cryptos') }}">Cryptos</a></li>
            </ul>
            <li><a class="dropdown-button" href="#!" data-activates="ext-managers-dropdown-mobile"><i class="material-icons left">build</i> Panels externes <i class="material-icons right">arrow_drop_down</i></a></li>
            <ul id="ext-managers-dropdown-mobile" class="dropdown-content">
              <li><a href="https://forum.icrypto.fr/admin.php">Panel Forum</a></li>
							<li><a href="https://coinhive.com/dashboard">Dashboard Coinhive</a></li>
              <li><a href="https://www.lordapo.com/login">Panel LordApo.com</a></li>
            </ul>
            <li><a href="{{ url('admin/users/show/'.Auth::user()->id) }}">{{ Auth::user()->name }}</a></li>
            <li><a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">Déconnexion</a></li>
            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">{{ csrf_field() }}</form>
          </ul>
        </div>
      </nav>
    </header>
    <main>
      @yield('content')
    </main>
		<footer class="page-footer indigo">
      <div class="footer-copyright footer-copyright-only">
        <div class="container">
        	© 2017 iCrypto.fr, <a class="white-text" href="https://www.lordapo.com">LordApo.com</a>.
        </div>
      </div>
    </footer>
  </body>

	<script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/js/materialize.min.js"></script>

	<script>
		$( document ).ready(function(){
			$(".button-collapse").sideNav();
			$('select').material_select();
			$('.modal').modal();
		})

		@foreach (Alert::all() as $alert)
		  Materialize.toast('{{ $alert }}', 5000);
		@endforeach

		@if($errors->any())
			@foreach ($errors->all() as $error)
				Materialize.toast('{{ $error }}', 5000);
			@endforeach
		@endif
	</script>
  @yield('add_js')
</html>
