@extends('layouts.frontend')

@section('title', 'Contact')

@section('content')
<div class="section">
  <div class="container">
    <h1 class="header center blue-grey-text">Contact</h1>
  </div>
</div>

<div class="section">
  <div class="container">
    <div class="row">
      <form class="col s12 m8 l6 offset-m2 offset-l3" method="POST">
        {{ csrf_field() }}
        @guest
        <div class="input-field col s12 m6">
          <i class="material-icons prefix">account_circle</i>
          <input type="text" minlength="3" maxlength="32" name="username" class="validate" required>
          <label for="username">Nom</label>
        </div>
        <div class="input-field col s12 m6">
          <i class="material-icons prefix">mail</i>
          <input type="email" maxlength="255" name="email" class="validate" required>
          <label for="email">Adresse email</label>
        </div>
        @else
        <div class="input-field col s12 m6">
          <i class="material-icons prefix">account_circle</i>
          <input type="text" minlength="3" maxlength="32" name="username" class="validate" value="{{ Auth::user()->name }}" disabled>
          <label for="username">Nom</label>
        </div>
        <div class="input-field col s12 m6">
          <i class="material-icons prefix">mail</i>
          <input type="email" maxlength="255" name="email" class="validate" value="{{ Auth::user()->email }}" disabled>
          <label for="email">Adresse email</label>
        </div>
        @endguest
        <div class="input-field col s12">
          <i class="material-icons prefix">message</i>
          <textarea class="materialize-textarea" minlength="30" maxlength="1000" data-length="1000" name="message" required></textarea>
          <label for="message">Message</label>
        </div>
        <div class="input-field center col s12">
          <button class="btn waves-effect waves-light" type="submit">Envoyer <i class="material-icons left">send</i></button>
          <a class="btn waves-effect waves-light red" href="{{ url('contact') }}">Effacer <i class="material-icons right">delete</i></a>
        </div>
      </form>
    </div>
  </div>
</div>
@endsection
