<!DOCTYPE html>
<html lang="fr">
	<head>
		<meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <meta name="csrf-token" content="{{ csrf_token() }}">
		<meta name="author" content="LordApo.com">
		<meta name="robots" content="NOINDEX">

		<title>iCrypto | Erreur 404</title>

		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/css/materialize.min.css">
		<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Raleway|Material+Icons">
		<link rel="stylesheet" href="{{ url('assets/css/style.css') }}">
	</head>
  <body>
    <header>
      <nav class="blue-grey">
        <div class="nav-wrapper">
          <a href="{{ url('/') }}" class="brand-logo center">iCrypto.fr</a>
        </div>
      </nav>
    </header>
    <main>
			<div class="section">
			  <div class="container">
			    <h1 class="header center blue-grey-text">Erreur 404</h1>
			  </div>
			</div>
			<div class="section">
			  <div class="container">
			    <div class="row">
			      <div class="col s12 m8 l6 offset-m2 offset-l3">
			        <h2>Page ou ressource non trouvée.</h2>
			        <p>La page ou ressource demandée n'existe pas, n'hésitez pas à contacter le webmaster si vous pensez qu'il sagit d'une erreur de notre part.</p>
			        <a class="btn waves-effect waves-light" href="{{ url('/') }}">Retourner à l'accueil</a>
			        <a class="btn waves-effect waves-light" href="https://www.lordapo.com/contact">Contacter le webmaster</a>
			      </div>
			    </div>
			  </div>
			</div>
    </main>
    <footer class="page-footer blue-grey">
      <div class="footer-copyright footer-copyright-only">
        <div class="container">
          &copy; {{ (date('Y') == '2017') ? '2017' : '2017-'.date('Y') }} iCrypto.fr, <a class="grey-text text-lighten-3" href="https://www.lordapo.com">LordApo.com</a>.
        </div>
      </div>
    </footer>
  </body>

	<script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/js/materialize.min.js"></script>
	<script>
		$( document ).ready(function(){
			$(".button-collapse").sideNav();
		})
	</script>
</html>
