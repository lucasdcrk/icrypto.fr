@extends('layouts.frontend')

@section('title', 'Coming Soon')

@section('content')
<div class="section">
  <div class="container">
    <h1 class="header center blue-grey-text">Coming Soon</h1>
  </div>
</div>

<div class="section">
  <div class="container">
    <div class="row">
      <div class="col s12 m8 l6 offset-m2 offset-l3">
        <h2>Bientôt disponible.</h2>
        <p>La page ou fonctionalité demandée n'est pas encore disponible, soyez patient :)</p>
        <a class="btn waves-effect waves-light" href="{{ url('/') }}">Retourner à l'accueil</a>
        <a class="btn waves-effect waves-light" href="https://www.lordapo.com/contact">Contacter le webmaster</a>
      </div>
    </div>
  </div>
</div>
@endsection
