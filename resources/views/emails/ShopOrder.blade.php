<h1>Bonjour,</h1>
Vous avez reçu une nouvelle commande sur la boutique <a href="https://www.icrypto.fr">iCrypto</a>.
<hr>

<p><b>Utilisateur :</b> {{ $name }}</p>
<p><b>Adresse email :</b> {{ $email }}</p>
<p><b>Solde :</b> {{ $balance }}</p>
<p><b>Coinhive :</b> {{ $coinhive }}</p>
<p><b>Type :</b> {{ $type }}</p>
<p><b>Cryptocurrency :</b> {{ $currency }}</p>
<p><b>Montant :</b> {{ $amount }}</p>
<p><b>Adresse :</b> {{ $address }}</p>
<p><b>Date :</b> {{ date('d-m-Y H:i:s') }}</p>
