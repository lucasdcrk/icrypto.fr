<h1>Bonjour,</h1>
Vous avez reçu un message sur <a href="https://www.icrypto.fr">iCrypto</a>.
<hr>

<p><b>Nom :</b> {{ $username }}</p>
<p><b>Adresse email :</b> {{ $email }}</p>
<p><b>Message :</b> {{ $messageContact }}</p>
