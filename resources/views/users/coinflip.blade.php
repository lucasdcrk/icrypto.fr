@extends('layouts.frontend')

@section('title', 'Coinflip')

@section('content')
<div class="section">
  <div class="container center">
    @if(isset($coinflip->state))
      @if($coinflip->state == 1)
      <h1 class="header green-text">Coinflip</h1>
      @else
      <h1 class="header red-text">Coinflip</h1>
      @endif
    @else
    <h1 class="header blue-grey-text">Coinflip</h1>
    @endif
    <p class="flow-text">Vous avez <b>{{ Auth::user()->balance }} PROTONS</b> disponibles.</p>
  </div>
</div>

<div class="section">
  <div class="container">
    <div class="row">
      <div class="col s12 m4 offset-m4">
        <div class="card blue-grey">
          <div class="card-content white-text">
            <span class="card-title">Coinflip</span>
              @if(Auth::user()->balance != 0)
              <div class="row">
                <form id="coinflip" method="POST">
                  {{ csrf_field() }}
                  <div class="input-field col s5">
                    <input type="number" id="amount" step="0.01" min="0.01" max="{{ Auth::user()->balance }}" name="amount" value="{{ (isset($coinflip->amount) ? $coinflip->amount : '') }}" class="validate" required>
                    <label for="amount">Mise (en protons)</label>
                  </div>
                  <div class="input-field col s7">
                    <button type="submit" class="btn waves-effect waves-light"><i class="material-icons left">gamepad</i>Jouer</button>
                  </div>
                  <div class="input-field col s12">
                    <button type="button" class="btn waves-effect waves-light green" onclick="min()">Min</button>
                    <button type="button" class="btn waves-effect waves-light blue" onclick="divideBy2()">/2</button>
                    <button type="button" class="btn waves-effect waves-light blue" onclick="multiplyBy2()">x2</button>
                    <button type="button" class="btn waves-effect waves-light red" onclick="max()">Max</button>
                  </div>
                </form>
              </div>
            </div>
            @else
            <h3>Coinflip indisponible</h3>
            <p class="flow-text">Désolé, vous n'avez aucun PROTON disponible pour jouer.</p>
            @endif
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

</div class="section">
  <div class="center">
    <h4>Autres moyens de gagner des PROTONS</h4>
    <a class="btn waves-effect waves-light btn-large" href="{{ url('games/gambling') }}"><i class="material-icons left">gamepad</i> Gambling</a>
    <a class="btn waves-effect waves-light btn-large" href="{{ url('faucet') }}"><i class="material-icons left">redeem</i> Faucet</a>
    <a class="btn waves-effect waves-light btn-large" href="{{ url('mine') }}"><i class="material-icons left">cloud</i> Miner (Coinhive)</a>
  </div>
</div>
@endsection

@section('add_js')
<script>
function min() {
  document.getElementById('amount').value = '0.01';
  Materialize.updateTextFields();
}

function max() {
  document.getElementById('amount').value = {{ Auth::user()->balance }};
  Materialize.updateTextFields();
}

function divideBy2() {
  var amount = Number($('#amount').val());
  document.getElementById('amount').value = (amount / 2).toFixed(2);
  Materialize.updateTextFields();
}

function multiplyBy2() {
  var amount = Number($('#amount').val());
  document.getElementById('amount').value = (amount * 2).toFixed(2);
  Materialize.updateTextFields();
}

@if(isset($coinflip->state))
  @if($coinflip->state == 1)
  var audio = new Audio('{{ url('assets/sound/ting.mp3') }}');
  audio.play();
  @else
  var audio = new Audio('{{ url('assets/sound/cowbell.mp3') }}');
  audio.play();
  @endif
@endif
</script>
@endsection
