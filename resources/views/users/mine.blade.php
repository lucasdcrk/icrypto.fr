@extends('layouts.frontend')

@section('title', 'Miner')

@section('content')
<div class="section">
  <div class="container">
    <div class="center">
      <h1 class="header blue-grey-text">Miner</h1>
      <p>10k HASHs = 1 Proton</p>
    </div>
  </div>
</div>

<div class="section">
  <div class="container">
    <div class="row">
      <div class="col s12 m8 l6 offset-m2 offset-l3 center">
        <h3>Miner des protons</h3>
        <div class="coinhive-miner" data-user="{{ Auth::user()->name }}" data-whitelabel="true" style="height: 200px" data-key="2y4L4G4O9ItZbw6KNWzAyzb5fvLlNSzG">
          <p>
            <b>Chargement en cours ...</b>
            <div class="progress">
              <div class="indeterminate"></div>
            </div>
            Si l'interface ne charge pas, <b>désactivez AdBlock</b>.
          </p>
        </div>
        <p class="flow-text">Vos PROTONS peuvent mettre jusqu'à 2 heures avant d'être crédités sur votre compte iCrypto.</p>
      </div>
    </div>
  </div>
</div>

</div class="section">
  <div class="center">
    <h4>Autres moyens de gagner des PROTONS</h4>
    <a class="btn waves-effect waves-light btn-large" href="{{ url('faucet') }}"><i class="material-icons left">redeem</i> Faucet</a>
    <a class="btn waves-effect waves-light btn-large" href="{{ url('games/coinflip') }}"><i class="material-icons left">gamepad</i> Coinflip</a>
    <a class="btn waves-effect waves-light btn-large" href="{{ url('games/gambling') }}"><i class="material-icons left">gamepad</i> Gambling</a>
  </div>
</div>
@endsection

@section('add_js')
<script src="https://coinhive.com/lib/miner.min.js" async></script>
@endsection
