@extends('layouts.frontend')

@section('title', 'Gambling')

@section('content')
<div class="section">
  <div class="container center">
    @if(isset($gambling->state))
      @if($gambling->state == 1)
      <h1 class="header green-text">Gambling</h1>
      @else
      <h1 class="header red-text">Gambling</h1>
      @endif
    @else
    <h1 class="header blue-grey-text">Gambling</h1>
    @endif
    <p class="flow-text">Vous avez <b>{{ Auth::user()->balance }} PROTONS</b> disponibles.</p>
  </div>
</div>

<div class="section">
  <div class="container">
    <div class="row">
      <div class="col s12 m4 offset-m4">
        <div class="card blue-grey">
          <div class="card-content white-text">
            <span class="card-title">Gambling</span>
              @if(1+1==0)
              <div class="row">
                <form id="Gambling" method="POST">
                  {{ csrf_field() }}
                  <div class="input-field col s12 m6">
                    <input type="number" id="amount" step="0.01" min="0.01" max="{{ Auth::user()->balance }}" name="amount" value="{{ (isset($gambling->amount) ? $gambling->amount : '') }}" class="validate" required>
                    <label for="amount">Bet</label>
                  </div>
                  <div class="input-field col s12 m6">
                    <input type="number" id="payout" min="1" max="100" name="payout" value="{{ (isset($gambling->payout) ? $gambling->payout : '') }}" class="validate" required>
                    <label for="payout">Payout (%)</label>
                  </div>
                  <div class="input-field col s12">
                    <button type="submit" class="btn waves-effect waves-light"><i class="material-icons left">gamepad</i>Jouer</button>
                  </div>
                </form>
              </div>
            </div>
            @else
            <h3>Gambling indisponible</h3>
            @endif
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

</div class="section">
  <div class="center">
    <h4>Autres moyens de gagner des PROTONS</h4>
    <a class="btn waves-effect waves-light btn-large" href="{{ url('games/coinflip') }}"><i class="material-icons left">gamepad</i> Coinflip</a>
    <a class="btn waves-effect waves-light btn-large" href="{{ url('faucet') }}"><i class="material-icons left">redeem</i> Faucet</a>
    <a class="btn waves-effect waves-light btn-large" href="{{ url('mine') }}"><i class="material-icons left">cloud</i> Miner (Coinhive)</a>
  </div>
</div>
@endsection

@section('add_js')
<script>
function min() {
  document.getElementById('amount').value = '0.01';
  Materialize.updateTextFields();
}

function max() {
  document.getElementById('amount').value = {{ Auth::user()->balance }};
  Materialize.updateTextFields();
}

function divideBy2() {
  var amount = Number($('#amount').val());
  document.getElementById('amount').value = (amount / 2).toFixed(2);
  Materialize.updateTextFields();
}

function multiplyBy2() {
  var amount = Number($('#amount').val());
  document.getElementById('amount').value = (amount * 2).toFixed(2);
  Materialize.updateTextFields();
}

@if(isset($gambling->state))
  @if($gambling->state == 1)
  var audio = new Audio('{{ url('assets/sound/ting.mp3') }}');
  audio.play();
  @else
  var audio = new Audio('{{ url('assets/sound/cowbell.mp3') }}');
  audio.play();
  @endif
@endif
</script>
@endsection
