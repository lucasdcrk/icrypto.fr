@extends('layouts.frontend')

@section('title', 'Faucet')

@section('content')
<div class="section">
  <div class="container">
    <h1 class="header center blue-grey-text">Faucet</h1>
  </div>
</div>

<div class="section">
  <div class="container">
    <div class="row">
      @if($nextFaucet < $now)
      <div class="col s12 m8 l6 offset-m2 offset-l3 center">
        <h3>Le faucet est disponible pour vous !</h3>
        <p class="flow-text">Vous pouvez obtenir 1 PROTON chaque heure en visitant cette page.</p>
        <form method="POST">
          {{ csrf_field() }}
          <div class="input-field center col s12">
            <button class="btn waves-effect waves-light" type="submit">Obtenir 1 PROTON</button>
          </div>
        </form>
      </div>
      @else
      <div class="col s12 m8 l6 offset-m2 offset-l3 center">
        <h3>Votre prochain faucet sera bientôt disponible !</h3>
        <p><b>Dernier faucet :</b> {{ $lastFaucet }}</p>
        <p><b>Prochain faucet :</b> {{ $nextFaucet }}</p>
      </div>
      @endif
    </div>
  </div>
</div>

</div class="section">
  <div class="center">
    <h4>Autres moyens de gagner des PROTONS</h4>
    <a class="btn waves-effect waves-light btn-large" href="{{ url('mine') }}"><i class="material-icons left">cloud</i> Miner (Coinhive)</a>
    <a class="btn waves-effect waves-light btn-large" href="{{ url('games/coinflip') }}"><i class="material-icons left">gamepad</i> Coinflip</a>
    <a class="btn waves-effect waves-light btn-large" href="{{ url('games/gambling') }}"><i class="material-icons left">gamepad</i> Gambling</a>
  </div>
</div>
@endsection
