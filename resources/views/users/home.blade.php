@extends('layouts.frontend')

@section('title', 'Mon compte')

@section('content')
<div class="section">
  <div class="container">
    <h1 class="header center blue-grey-text">Bonjour, {{ Auth::user()->name }}</h1>
  </div>
</div>

<div class="section">
  <div class="container">
    <div class="row">
      <div class="col s12 m5">
        <h3>Votre compte :</h3>
        <p><b>Nom d'utilisateur :</b> {{ Auth::user()->name }}</p>
        <p><b>Adresse email :</b> {{ Auth::user()->email }}</p>
        <p><b>Compte crée le :</b> {{ Auth::user()->created_at }}</p>
        <a class="dropdown-button btn" href="#" data-activates="account-settings-dropdown">Gestion de votre compte</a>
        <ul id="account-settings-dropdown" class="dropdown-content">
          <li><a href="{{ url('account/settings') }}"><i class="material-icons left">settings</i> Paramètres</a></li>
          <li class="divider"></li>
          <li><a class="red-text" href="{{ url('account/delete') }}"><i class="material-icons left">delete</i> Supprimer</a></li>
        </ul>
      </div>
      <div class="col s12 m4">
        <h3>Votre Solde :</h3>
        <h5>Vous avez : {{ Auth::user()->balance }} PROTONS</h5>
        <a class="dropdown-button btn" href="#" data-activates="account-balance-dropdown">Obtenir des protons</a>
        <ul id="account-balance-dropdown" class="dropdown-content">
          <li><a href="{{ url('faucet') }}"><i class="material-icons left">redeem</i> Faucet</a></li>
          <li><a href="{{ url('mine') }}"><i class="material-icons left">cloud</i> Miner</a></li>
          <li class="divider"></li>
          <li><a href="{{ url('games/coinflip') }}"><i class="material-icons left">gamepad</i> Coinflip</a></li>
          <li><a href="{{ url('games/gambling') }}"><i class="material-icons left">gamepad</i> Gambling</a></li>
        </ul>
      </div>
      <div class="col s12 m3">
        <h3>Votre Avatar :</h3>
        <a href="https://gravatar.com"><img src="https://gravatar.com/avatar/{{ md5(Auth::user()->email) }}?s=250" alt="Avatar de {{ Auth::user()->name }}"></a>
        <p>Les avatars d'iCrypto sont gérés par Gravatar.com.</p>
      </div>
    </div>
  </div>
</div>
@endsection
