@extends('layouts.frontend')

@section('title', 'Paramètres')

@section('content')
<div class="section">
  <div class="container">
    <h1 class="header center blue-grey-text">{{ Auth::user()->name }} > Paramètres</h1>
  </div>
</div>

<div class="section">
  <div class="container">
    <div class="row">
      <div class="col s12 m4">
        <h3>Compte :</h3>
        <div class="col s12">
          <form method="POST" action="{{ url('account/settings/email') }}">
            {{ csrf_field() }}
            <div class="input-field col s12">
              <i class="material-icons prefix">face</i>
              <input type="text" name="name" value="{{ Auth::user()->name }}" disabled>
              <label for="name">Nom d'utilisateur</label>
            </div>
            <div class="input-field col s12">
              <i class="material-icons prefix">mail</i>
              <input type="email" maxlength="255" name="email" class="validate" value="{{ Auth::user()->email }}" required>
              <label for="email">Adresse email</label>
            </div>
            <div class="input-field col s12">
              <i class="material-icons prefix">view_agenda</i>
              <input type="text" name="created_at" value="{{ Auth::user()->created_at }}" disabled>
              <label for="created_at">Date d'inscription</label>
            </div>
            <div class="input-field center col s12">
              <button class="btn waves-effect waves-light" type="submit"><i class="material-icons left">check</i> Valider</button>
            </div>
          </form>
        </div>
      </div>
      <div class="col s12 m4">
        <h3>Mot de passe :</h3>
        <div class="col s12">
          <form method="POST" action="{{ url('account/settings/password') }}">
            {{ csrf_field() }}
            <div class="input-field col s12">
              <i class="material-icons prefix">lock_outline</i>
              <input type="password" name="password" class="validate" required>
              <label for="password">Mot de passe actuel</label>
            </div>
            <div class="input-field col s12">
              <i class="material-icons prefix">lock</i>
              <input type="password" minlength="8" name="newPassword" class="validate" required>
              <label for="newPassword">Nouveau mot de passe</label>
            </div>
            <div class="input-field col s12">
              <i class="material-icons prefix">lock</i>
              <input type="password" minlength="8" name="newPassword_confirmation" class="validate" required>
              <label for="newPassword_confirmation">Nouveau mot de passe (confirmation)</label>
            </div>
            <div class="input-field center col s12">
              <button class="btn waves-effect waves-light" type="submit"><i class="material-icons left">check</i> Valider</button>
            </div>
          </form>
        </div>
      </div>
      <div class="col s12 m4">
        <h3>Coinhive :</h3>
        <p>Coinhive vous permet de remporter des PROTONS en minant du XMR sur votre navigateur. Vos PROTONS seront automatiquements crédités environs toutes les heures.</p>
        <form method="POST" action="{{ url('account/settings/coinhive') }}">
          {{ csrf_field() }}
          <div class="input-field col s12">
            <i class="material-icons prefix">tune</i>
            <input type="number" min="0" max="8" class="validate" name="coinhive-threads" value="{{ Auth::user()->coinhive }}" required>
            <label for="coinhive-threads">Nombre de threads (0 = désactivé) :</label>
          </div>
          <p><b>Attention : Un valeur trop élevée peut entrainer une inaccessibilité du site.</b> Nous vous conseillons 8 threads pour un desktop, 4 pour un laptop et 2 pour un mobile.</p>
          <div class="input-field center col s12">
            <button type="submit" class="btn waves-effect waves-light"><i class="material-icons left">check</i> Valider</button>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>

</div class="section">
  <div class="center">
    <a class="btn waves-effect waves-light btn-large" href="{{ url('account') }}" style="margin-top: 1rem;"><i class="material-icons left">arrow_back</i> Retour à mon compte</a>
  </div>
</div>
@endsection
