@extends('layouts.frontend')

@section('title', 'Mot de passe oublié')

@section('content')
<div class="section">
  <div class="container">
    <h1 class="header center red-text">Mot de passe oublié</h1>
    <p class="center">Vous recevrez un lien de réinitialisation de votre mot de passe par email.</p>
    @if (session('status'))
      <p class="center">
        {{ session('status') }}
      </p>
    @endif
  </div>
</div>

<div class="section">
  <div class="container">
    <div class="row">
      <div class="col s12 m8 l6 offset-m2 offset-l3">
        <form method="POST" action="{{ route('password.email') }}">
          {{ csrf_field() }}
          <div class="input-field col s12">
            <i class="material-icons prefix">mail</i>
            <input type="email" maxlength="255" name="email" class="validate" value="{{ old('email') }}" required>
            <label for="email">Adresse email</label>
          </div>
          <div class="input-field center col s12">
            <button class="btn waves-effect waves-light" type="submit"><i class="material-icons left">check</i> Valider</button>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>

<div class="section">
  <div class="container">
    <div class="center">
      <p>Autre options disponibles :</p>
      <a class="btn waves-effect waves-light blue" href="{{ route('login') }}"><i class="material-icons left">arrow_back</i> Retour à la connexion</a>
    </div>
  </div>
</div>
@endsection
