@extends('layouts.frontend')

@section('title', 'Inscription')

@section('content')
<div class="section">
  <div class="container">
    <h1 class="header center blue-grey-text">Inscription</h1>
  </div>
</div>

<div class="section">
  <div class="container">
    <div class="row">
      <div class="col s10 m6 l4 offset-s1 offset-m3 offset-l4">
        <form method="POST" action="{{ route('register') }}">
          {{ csrf_field() }}
          <div class="input-field col s12">
            <i class="material-icons prefix">face</i>
            <input type="text" min="3" max="16" data-length="16" name="name" class="validate" value="{{ old('name') }}" required>
            <label for="name">Nom d'utilisateur</label>
          </div>
          <div class="input-field col s12">
            <i class="material-icons prefix">mail</i>
            <input type="email" maxlength="255" name="email" class="validate" value="{{ old('email') }}" required>
            <label for="email">Adresse email</label>
          </div>
          <div class="input-field col s12">
            <i class="material-icons prefix">lock</i>
            <input type="password" min="8" name="password" class="validate" required>
            <label for="password">Mot de passe</label>
          </div>
          <div class="input-field col s12">
            <i class="material-icons prefix">lock</i>
            <input type="password" name="password_confirmation" class="validate" required>
            <label for="password_confirmation">Mot de passe (confirmation)</label>
          </div>
          <div class="input-field center col s12">
            <button class="btn waves-effect waves-light" type="submit"><i class="material-icons left">check</i> Valider</button>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>

<div class="section">
  <div class="container">
    <div class="center">
      <p>Autres options disponibles :</p>
      <a class="btn waves-effect waves-light blue" href="{{ route('login') }}">J'ai déjà un compte</a>
    </div>
  </div>
</div>
@endsection
