@extends('layouts.frontend')

@section('title', 'Connexion')

@section('content')
<div class="section">
  <div class="container">
    <h1 class="header center blue-grey-text">Connexion</h1>
  </div>
</div>

<div class="section">
  <div class="container">
    <div class="row">
      <div class="col s10 m6 l4 offset-s1 offset-m3 offset-l4">
        <form method="POST" action="{{ route('login') }}">
          {{ csrf_field() }}
          <div class="input-field col s12">
            <i class="material-icons prefix">mail</i>
            <input type="email" maxlength="255" name="email" class="validate" value="{{ old('email') }}" required>
            <label for="email">Adresse email</label>
          </div>
          <div class="input-field col s12">
            <i class="material-icons prefix">lock</i>
            <input type="password" name="password" class="validate" required>
            <label for="password">Mot de passe</label>
          </div>
          <div class="col 12">
            <p>
              <input type="checkbox" class="filled-in" id="remember" {{ old('remember') ? 'checked="checked"' : '' }}>
              <label for="remember">Rester connecté</label>
            </p>
          </div>
          <div class="input-field center col s12">
            <button class="btn waves-effect waves-light" type="submit"><i class="material-icons left">check</i> Valider</button>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>

<div class="section">
  <div class="container">
    <div class="center">
      <p>Autres options disponibles :</p>
      <a class="btn waves-effect waves-light red" href="{{ route('password.request') }}">Mot de passe oublié</a>
      <a class="btn waves-effect waves-light blue" href="{{ route('register') }}">Inscription</a>
    </div>
  </div>
</div>
@endsection
