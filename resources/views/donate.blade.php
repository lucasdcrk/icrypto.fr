@extends('layouts.frontend')

@section('title', 'Dons')

@section('content')
<div class="section">
  <div class="container">
    <h1 class="header center blue-grey-text">Faire un don</h1>
  </div>
</div>

<div class="section">
  <div class="container">
    <div class="row">
      <div class="col s12 m8 l6 offset-m2 offset-l3">
        <div class="center">
          <img height="250px" src="{{ url('assets/img/26772-full.jpg') }}">
        </div>
        <p>Nous investissons beaucoup de temps et d'argent sur notre projet. C'est pour cela que suite à des demandes de la communauté, nous avons ajouté un moyen pour vous de donner pour nous aider à financer le site, l'hebergement, etc.</p>
      </div>
    </div>
  </div>
</div>

<div class="section">
  <div class="container">
    <div class="row">
      <div class="col s12 m6 l4">
        <h1>PayPal</h1>
        <p><b>Email PayPal :</b> contact.icrypto@gmail.com</p>
      </div>
      <div class="col s12 m6 l4">
        <h1>Crypto</h1>
        <p><b>BTC :</b> 14V6i5xWBL5k4zY1V2qrV3p72nymrsr1rt</p>
        <p><b>ETH :</b> 0x62a76af984b4aa01df2a1e55f04290f857ed04e9</p>
        <p><b>OMG :</b> 0x60c34b4a597f61a6463f7aafd02c556702dbc6d7</p>
        <p><b>LTC :</b> LftqUDJuLw3Ycho1SvmZx9myg4H5RB5qYz</p>
      </div>
      <div class="col s12 m6 l4">
        <h1>Coinhive</h1>
        <div class="col s12 m10">
          <script src="https://coinhive.com/lib/miner.min.js" async></script>
          <div class="coinhive-miner" {{ (Auth::check()) ? 'data-user="Auth::user()->name"' : '' }} data-whitelabel="true" style="width: 256px; height: 310px" data-key="2y4L4G4O9ItZbw6KNWzAyzb5fvLlNSzG">
            <p>
              <b>Chargement en cours ...</b>
              <div class="progress">
                <div class="indeterminate"></div>
              </div>
              Si l'interface ne charge pas, <b>désactivez AdBlock</b>.
            </p>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
