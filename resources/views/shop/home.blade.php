@extends('layouts.frontend')

@section('title', 'Shop')

@section('content')
<div class="section">
  <div class="container">
    <h1 class="header center blue-grey-text">Boutique</h1>
  </div>
</div>

<div class="section">
  <div class="container">
    <div class="row">
      <div class="col s12 m8 l6 offset-m2 offset-l3">
        <div class="card">
          <div class="card-tabs">
            <ul class="tabs tabs-fixed-width blue-grey tabs-transparent">
              <li class="tab white-text"><a class="active" href="#buy">Acheter Crypto</a></li>
              <li class="tab white-text"><a href="#sell">Vendre Crypto</a></li>
            </ul>
          </div>
          <div class="card-content grey lighten-4">
            <div id="buy">
              <h3>Acheter de la crypto</h3>
              <div class="row">
                <form method="POST" action="{{ url('shop/buy/crypto') }}">
                  {{ csrf_field() }}
                  <div class="input-field col s12 m6">
                    <i class="material-icons prefix">attach_money</i>
                    <input type="number" step="0.00000001" min="0.00000100" id="buyAmount" name="amount" value="0.00000100" class="validate" required>
                    <label for="amount">Montant</label>
                  </div>
                  <div class="input-field col s12 m6">
                    <select name="currency" id="buyCrypto" required>
                      <option value="" disabled selected>Choisissez une crypto</option>
                      @foreach($sell_cryptos as $sell_crypto)
                      <option value="{{ $sell_crypto->id }}">{{ $sell_crypto->name }} (Frais : {{ $sell_crypto->send_fees }}€ + {{ $sell_crypto->buy_fees }}%)</option>
                      @endforeach
                    </select>
                    <label>Monnaie</label>
                  </div>
                  <div class="input-field col s12">
                    <i class="material-icons prefix">insert_link</i>
                    <input type="text" name="address" class="validate" required>
                    <label for="address">Adresse Wallet</label>
                  </div>
                  <div class="input-field col s12">
                    <button type="submit" class="btn waves-effect waves-light"><i class="material-icons left">check</i> Valider</button>
                  </div>
                </form>
              </div>
            </div>
            <div id="sell">
              <div class="row">
                <h3>Vendre de la crypto</h3>
                <form method="POST" action="{{ url('shop/sell/crypto') }}">
                  {{ csrf_field() }}
                  <div class="input-field col s12 m6">
                    <i class="material-icons prefix">attach_money</i>
                    <input type="number" step="0.00000001" min="0.00000100" id="buyAmount" name="amount" value="0.00000100" class="validate" required>
                    <label for="amount">Montant</label>
                  </div>
                  <div class="input-field col s12 m6">
                    <select name="currency" id="buyCrypto" required>
                      <option value="" disabled selected>Choisissez une crypto</option>
                      @foreach($buy_cryptos as $buy_crypto)
                      <option value="{{ $buy_crypto->id }}">{{ $buy_crypto->name }} (Frais : {{ $buy_crypto->buy_fees }}%)</option>
                      @endforeach
                    </select>
                    <label>Monnaie désirée</label>
                  </div>
                  <div class="input-field col s12">
                    <i class="material-icons prefix">insert_link</i>
                    <input type="email" name="address" class="validate" required>
                    <label for="address">Adresse PayPal</label>
                  </div>
                  <div class="input-field col s12">
                    <button type="submit" class="btn waves-effect waves-light"><i class="material-icons left">check</i> Valider</button>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
