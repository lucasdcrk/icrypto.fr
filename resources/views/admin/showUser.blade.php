@extends('layouts.backend')

@section('title', 'Info utilisateur')

@section('content')
<div class="section">
  <div class="container">
    <h1 class="header center indigo-text">Compte de {{ $user->name }} <a class="btn waves-effect waves-light" href="{{ url('admin/users/edit/'.$user->id) }}">Editer</a></h1>
  </div>
</div>

<div class="section">
  <div class="container">
    <div class="row">
      <div class="col s12 m4 offset-m4">
        <div class="input-field col s12">
          <i class="material-icons prefix">bug_report</i>
          <input type="number" value="{{ $user->id }}" disabled>
          <label for="name">ID</label>
        </div>
        <div class="input-field col s12">
          <i class="material-icons prefix">face</i>
          <input type="text" name="name" value="{{ $user->name }}" disabled>
          <label for="name">Nom d'utilisateur</label>
        </div>
        <div class="input-field col s12">
          <i class="material-icons prefix">mail</i>
          <input type="email" value="{{ $user->email }}" disabled>
          <label for="email">Adresse email</label>
        </div>
        <div class="input-field col s12">
          <i class="material-icons prefix">attach_money</i>
          <input type="number" value="{{ $user->balance }}" disabled>
          <label for="balance">Solde</label>
        </div>
        <div class="input-field col s12">
          <i class="material-icons prefix">redeem</i>
          <input type="text" value="{{ $user->lastFaucet }}" disabled>
          <label for="lastFaucet">Dernier Faucet</label>
        </div>
        <div class="input-field col s12">
          <i class="material-icons prefix">cloud</i>
          <input type="number" value="{{ $user->coinhive }}" disabled>
          <label for="coinhive">Coinhive</label>
        </div>
      </div>
    </div>
  </div>
</div>

</div class="section">
  <div class="center">
    <a class="btn waves-effect waves-light btn-large" href="{{ url('admin/users') }}" style="margin-top: 1rem;"><i class="material-icons left">arrow_back</i> Retour aux utilisateurs</a>
  </div>
</div>
@endsection
