@extends('layouts.backend')

@section('title', 'Utilisateurs')

@section('content')
<div class="section">
  <div class="container">
    <h1 class="header center indigo-text">Utilisateurs</h1>
  </div>
</div>

<div class="section">
  <div class="container">
    <table class="responsive-table bordered highlight">
      <thead>
        <tr>
          <th>ID</th>
          <th>Nom</th>
          <th>Adresse email</th>
          <th>Admin</th>
          <th>Solde</th>
          <th>Coinhive</th>
          <th>Date d'inscription</th>
          <th>Actions</th>
        </tr>
      </thead>
      <tbody>
      @foreach(App\user::all() as $user)
      <tr>
        <td>{{ $user->id }}</td>
        <td>{{ $user->name }}</td>
        <td>{{ $user->email }}</td>
        <td>{{ ($user->isAdmin == 1) ? 'Oui' : 'Non' }}</td>
        <td>{{ $user->balance }} PROTONS</td>
        <td>{{ ($user->coinhive != 0) ? 'Oui ('.$user->coinhive.' threads)' : 'Non' }}</td>
        <td>{{ $user->created_at }}</td>
        <td>
          <a class="btn waves-effect waves-light blue" href="{{ url('admin/users/show/'.$user->id) }}">Info</a>
        </td>
      </tr>
      @endforeach
      </tbody>
    </table>
  </div>
</div>
@endsection
