@extends('layouts.backend')

@section('title', 'Info crypto')

@section('content')
<div class="section">
  <div class="container">
    <h1 class="header center indigo-text">Shop > Cryptos > {{ $crypto->name }} <a class="btn waves-effect waves-light" href="{{ url('admin/shop/cryptos/edit/'.$crypto->id) }}">Editer</a></h1>
  </div>
</div>

<div class="section">
  <div class="container">
    <div class="row">
      <div class="col s12 m4 offset-m4">
        <div class="input-field col s12">
          <input type="number" value="{{ $crypto->id }}" disabled>
          <label for="id">ID</label>
        </div>
        <div class="input-field col s12">
          <input type="text" value="{{ $crypto->name }}" disabled>
          <label for="name">Nom</label>
        </div>
        <div class="input-field col s12">
          <input type="text" value="{{ $crypto->slug }}" disabled>
          <label for="slug">Slug</label>
        </div>
        <div class="input-field col s12">
          <input type="text" value="{{ $crypto->send_fees }}$" disabled>
          <label for="send_fees">Frais d'envoi</label>
        </div>
        <div class="input-field col s12">
          <input type="text" value="{{ ($crypto->buy != 0) ? 'Activé' : 'Désactivé' }} ({{ $crypto->buy_fees }}% de frais)" disabled>
          <label for="buy">Achat</label>
        </div>
        <div class="input-field col s12">
          <input type="text" value="{{ ($crypto->sell != 0) ? 'Activée' : 'Désactivée' }} ({{ $crypto->sell_fees }}% de frais)" disabled>
          <label for="sell">Vente</label>
        </div>
        <div class="input-field col s12">
          <input type="text" value="{{ $crypto->created_at }}" disabled>
          <label for="created_at">Date d'ajout</label>
        </div>
        <div class="input-field col s12">
          <input type="text" value="{{ $crypto->updated_at }}" disabled>
          <label for="updated_at">Date de modification</label>
        </div>
      </div>
    </div>
  </div>
</div>

</div class="section">
  <div class="center">
    <a class="btn waves-effect waves-light btn-large" href="{{ url('admin/shop/cryptos') }}" style="margin-top: 1rem;"><i class="material-icons left">arrow_back</i> Retour aux cryptos</a>
  </div>
</div>
@endsection
