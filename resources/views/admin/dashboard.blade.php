@extends('layouts.backend')

@section('title', 'Dashboard')

@section('content')
<div class="section">
  <div class="container">
    <h1 class="header center indigo-text">Dashboard</h1>
  </div>
</div>

<div class="section">
  <div class="container">
    <div class="row">
      <div class="col s12 m3">
        <div class="card indigo darken-1">
          <div class="card-content white-text">
            <span class="card-title"><a class="white-text" href="{{ url('admin/users') }}">Utilisateurs</a></span>
            <p>Au total</p>
            <h1>{{ $usercount->total }}</h1>
          </div>
        </div>
      </div>
      <div class="col s12 m3">
        <div class="card indigo darken-1">
          <div class="card-content white-text">
            <span class="card-title">Inscrits</span>
            <p>Aujourd'hui</p>
            <h1>{{ $usercount->today }}</h1>
          </div>
        </div>
      </div>
      <div class="col s12 m3">
        <div class="card indigo darken-1">
          <div class="card-content white-text">
            <span class="card-title">Inscrits</span>
            <p>Cette semaine</p>
            <h1>{{ $usercount->thisWeek }}</h1>
          </div>
        </div>
      </div>
      <div class="col s12 m3">
        <div class="card indigo darken-1">
          <div class="card-content white-text">
            <span class="card-title">Inscrits</span>
            <p>Ce mois-ci</p>
            <h1>{{ $usercount->thisMonth }}</h1>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="section">
  <div class="container">
    <div class="row">
      <div class="col s12 m3">
        <div class="card indigo darken-1">
          <div class="card-content white-text">
            <span class="card-title">PROTONS</span>
            <p>En circulation</p>
            <h1>{{ $usercount->balance }}</h1>
          </div>
        </div>
      </div>
      <div class="col s12 m3">
        <div class="card indigo darken-1">
          <div class="card-content white-text">
            <span class="card-title"><a class="white-text" href="https://coinhive.com/dashboard">Coinhive</a></span>
            <p>Puissance de calcul</p>
            <h1>{{ round($usercount->coinhive, 2) }} H/s</h1>
          </div>
        </div>
      </div>
      <div class="col s12 m6">
        <div class="card indigo darken-1">
          <div class="card-content white-text">
            <span class="card-title"><a class="white-text" href="https://coinhive.com/dashboard">Coinhive</a></span>
            <p>Solde en attente</p>
            <h1>{{ round($usercount->coinhiveBalance, 8) }} XMR</h1>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
