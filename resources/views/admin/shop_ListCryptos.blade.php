@extends('layouts.backend')

@section('title', 'Shop : Cryptos')

@section('content')
<div class="section">
  <div class="container">
    <h1 class="header center indigo-text">Cryptos</h1>
  </div>
</div>

<div class="section">
  <div class="container">
    <table class="responsive-table bordered highlight">
      <thead>
        <tr>
          <th>ID</th>
          <th>Nom</th>
          <th>Slug</th>
          <th>Frais d'envoi</th>
          <th>Achat</th>
          <th>Vente</th>
          <th>Date de création</th>
          <th>Date de modification</th>
          <th>Actions</th>
        </tr>
      </thead>
      <tbody>
      @foreach($cryptos as $crypto)
      <tr>
        <td>{{ $crypto->id }}</td>
        <td>{{ $crypto->name }}</td>
        <td>{{ $crypto->slug }}</td>
        <td>{{ $crypto->send_fees }}$</td>
        <td>{{ ($crypto->buy != 0) ? 'Activé ('.$crypto->buy_fees.' % de frais)' : 'Désactivé' }}</td>
        <td>{{ ($crypto->sell != 0) ? 'Activé ('.$crypto->sell_fees.' % de frais)' : 'Désactivé' }}</td>
        <td>{{ $crypto->created_at }}</td>
        <td>{{ $crypto->updated_at }}</td>
        <td>
          <a class="btn waves-effect waves-light blue" href="{{ url('admin/shop/cryptos/show/'.$crypto->id) }}">Info</a>
        </td>
      </tr>
      @endforeach
      </tbody>
    </table>
  </div>
</div>
@endsection
