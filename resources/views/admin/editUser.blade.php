@extends('layouts.backend')

@section('title', 'Gestion des utilisateurs')

@section('content')
<div class="section">
  <div class="container">
    <h1 class="header center indigo-text">Modifier {{ $user->name }}</h1>
  </div>
</div>

<div class="section">
  <div class="container">
    <div class="row">
      <div class="col s12 m4 offset-m4">
        <form method="POST">
          {{ csrf_field() }}
          <div class="input-field col s12">
            <i class="material-icons prefix">face</i>
            <input type="text" min="3" max="16" name="name" class="validate" value="{{ $user->name }}" required>
            <label for="name">Nom d'utilisateur</label>
          </div>
          <div class="input-field col s12">
            <i class="material-icons prefix">mail</i>
            <input type="email" maxlength="255" name="email" class="validate" value="{{ $user->email }}" required>
            <label for="email">Adresse email</label>
          </div>
          <div class="input-field col s12">
            <i class="material-icons prefix">attach_money</i>
            <input type="number" step="0.01" min="0" name="balance" class="validate" value="{{ $user->balance }}" required>
            <label for="balance">Solde</label>
          </div>
          <div class="input-field col s12">
            <i class="material-icons prefix">redeem</i>
            <input type="text" name="lastFaucet" class="validate" value="{{ $user->lastFaucet }}" required>
            <label for="lastFaucet">Dernier Faucet</label>
          </div>
          <div class="input-field col s12">
            <i class="material-icons prefix">cloud</i>
            <input type="number" min="0" name="coinhive" class="validate" value="{{ $user->coinhive }}" required>
            <label for="coinhive">Coinhive</label>
          </div>
          <div class="input-field center col s12">
            <button class="btn waves-effect waves-light" type="submit"><i class="material-icons left">check</i> Valider</button>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>

</div class="section">
  <div class="center">
    <a class="btn waves-effect waves-light btn-large" href="{{ url('admin/users/show/'.$user->id) }}" style="margin-top: 1rem;"><i class="material-icons left">arrow_back</i> Retour</a>
  </div>
</div>
@endsection
