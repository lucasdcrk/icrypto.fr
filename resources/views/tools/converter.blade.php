@extends('layouts.frontend')

@section('title', 'Convertisseur')

@section('content')
<div class="section">
  <div class="container">
    <h1 class="header center blue-grey-text">Convertisseur</h1>
    <p class="center">Powered by LordApo.com API.</p>
  </div>
</div>

<div class="section">
  <div class="container">
    <h1 class="center">Nous mettons à jour notre système de conversion.</h1>
    <p class="flow-text center">Vous pourrez bientôt convertir des BTC / ETH / LTC / OMG / TUSD / $ USD / $ CAD / € EUR / £ GBP et pleins d'autres crypto-currencies, tout ça depuis un même interface.</p>
  </div>
</div>

<div class="section">
  <div class="container">
    <div class="center">
      <a class="btn btn-large waves-effect waves-light blue-grey" href="{{ url('tools/converter/old') }}">Accèder à l'ancienne version</a>
    </div>
  </div>
</div>
@endsection
