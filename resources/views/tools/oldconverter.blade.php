@extends('layouts.frontend')

@section('title', 'Convertisseur USD/BTC')

@section('content')
<div class="section">
  <div class="container">
    <h1 class="header center blue-grey-text">Convertisseur</h1>
    <p class="center">Ancienne version. Powered by BlockChain.info API.</p>
  </div>
</div>

<div class="section">
  <div class="container">
    <div class="row">
      <div class="col s12 m4 offset-m4">
        <div class="card">
          <div class="card-tabs">
            <ul class="tabs tabs-fixed-width blue-grey tabs-transparent">
              <li class="tab white-text"><a class="active" href="#btc">BTC -> Dollars</a></li>
              <li class="tab white-text"><a href="#usd">Dollars -> BTC</a></li>
            </ul>
          </div>
          <div class="card-content grey lighten-4">
            <div id="btc">
              <h5>Convertir des BTC en $</h5>
              <div class="row">
                <form id="c1">
                  <div class="input-field col s12">
                    <label for="c1-usd">Montant (en $) :</label>
                    <input type="number" id="c1-usd">
                  </div>
                  <div class="input-field col s12">
                    <input type="text" id="c1-btc" placeholder="0" disabled>
                    <label for="c1-btc">Valeur (en BTC) :</label>
                  </div>
                </form>
              </div>
            </div>
            <div id="usd">
              <h5>Convertir des $ en BTC</h5>
              <div class="row">
                <form id="c2">
                  <div class="input-field col s12">
                    <input type="number" id="c2-btc">
                    <label for="c2-btc">Montant (en BTC) :</label>
                  </div>
                  <div class="input-field col s12">
                    <input type="text" id="c2-usd" placeholder="0" disabled>
                    <label for="c2-usd">Valeur (en $) :</label>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="section">
  <div class="container">
    <div class="center">
      <a class="btn btn-large waves-effect waves-light blue-grey" href="{{ url('tools/converter') }}">Nouvelle version</a>
    </div>
  </div>
</div>
@endsection

@section('add_js')
<script>
var usd_btc_ratio;
$.getJSON("https://blockchain.info/tobtc?currency=USD&value=1", function(json){
  usd_btc_ratio = json;
});

$('input').keyup(function(){
  var c1_usd  = Number($('#c1-usd').val());

  document.getElementById('c1-btc').value = c1_usd * usd_btc_ratio;
});

$('input').keyup(function(){
  var c2_btc  = Number($('#c2-btc').val());

  document.getElementById('c2-usd').value = c2_btc * ( 1 / usd_btc_ratio );
});

$('input').change(function(){
  var c1_usd  = Number($('#c1-usd').val());

  document.getElementById('c1-btc').value = c1_usd * usd_btc_ratio;
});

$('input').change(function(){
  var c2_btc  = Number($('#c2-btc').val());

  document.getElementById('c2-usd').value = c2_btc * ( 1 / usd_btc_ratio );
});
</script>
@endsection
