@extends('layouts.frontend')

@section('title', 'Calculatrice')

@section('content')
<div class="section">
  <div class="container">
    <h1 class="header center blue-grey-text">Calculatrice</h1>
  </div>
</div>

<div class="section">
  <div class="container">
    <div class="row">
      <div class="col s12 m8 offset-m2">
        <div class="card">
          <div class="card-tabs">
            <ul class="tabs tabs-fixed-width blue-grey tabs-transparent">
              <li class="tab white-text"><a class="active" href="#sellPrice">prix de vente</a></li>
              <li class="tab white-text"><a href="#buyPrice">Prix d'achat</a></li>
              <li class="tab white-text"><a href="#profitPercentage">Pourcentage bénéfices</a></li>
            </ul>
          </div>
          <div class="card-content grey lighten-4">
            <div id="sellPrice">
              <h5>Calculer un prix de vente</h5>
              <div class="row">
                <form onsubmit="event.preventDefault(); calculateSellPrice()">
                  <div class="input-field col s12 m6">
                    <i class="material-icons prefix">attach_money</i>
                    <input type="number" id="buyPrice1" step="0.01" min="0" class="validate" required>
                    <label for="buyPrice1">Prix d'achat</label>
                  </div>
                  <div class="input-field col s12 m6">
                    <i class="material-icons prefix">pie_chart</i>
                    <input type="number" id="profitPercentage1" step="0.01" min="0.01" class="validate" required>
                    <label for="profitPercentage1">Pourcentage de bénéfice voulu</label>
                  </div>
                  <p class="flow-text" id="sellPriceResult"></p>
                  <div class="input-field col s12">
                    <button class="btn waves-effect waves-light"><i class="material-icons left">check</i> Calculer</button>
                  </div>
                </form>
              </div>
            </div>
            <div id="buyPrice">
              <h5>Calculer un prix d'achat</h5>
              <div class="row">
                <form onsubmit="event.preventDefault(); calculateBuyPrice()">
                  <div class="input-field col s12 m6">
                    <i class="material-icons prefix">attach_money</i>
                    <input type="number" id="sellPrice2" step="0.01" min="0" class="validate" required>
                    <label for="sellPrice2">Prix de vente</label>
                  </div>
                  <div class="input-field col s12 m6">
                    <i class="material-icons prefix">pie_chart</i>
                    <input type="number" id="profitPercentage2" step="0.01" min="0.01" class="validate" required>
                    <label for="profitPercentage2">Pourcentage de bénéfice voulu</label>
                  </div>
                  <p class="flow-text" id="buyPriceResult"></p>
                  <div class="input-field col s12">
                    <button class="btn waves-effect waves-light"><i class="material-icons left">check</i> Calculer</button>
                  </div>
                </form>
              </div>
            </div>
            <div id="profitPercentage">
              <h5>Calculer un pourcentage de bénéfice</h5>
              <div class="row">
                <form onsubmit="event.preventDefault(); calculateProfitPercentage()">
                  <div class="input-field col s12 m6">
                    <i class="material-icons prefix">attach_money</i>
                    <input type="number" id="buyPrice3" step="0.01" min="0" class="validate" required>
                    <label for="buyPrice3">Prix d'achat</label>
                  </div>
                  <div class="input-field col s12 m6">
                    <i class="material-icons prefix">attach_money</i>
                    <input type="number" id="sellPrice3" step="0.01" min="0" class="validate" required>
                    <label for="sellPrice3">Prix de vente</label>
                  </div>
                  <p class="flow-text" id="profitPercentageResult"></p>
                  <div class="input-field col s12">
                    <button class="btn waves-effect waves-light"><i class="material-icons left">check</i> Calculer</button>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection

@section('add_js')
<script>
function calculateSellPrice() {
  var buyPrice = Number($('#buyPrice1').val());
  var profitPercentage = Number($('#profitPercentage1').val());
  var result = buyPrice + buyPrice * profitPercentage / 100;
  $("#sellPriceResult").html("Vous devez vendre à " + result + "{{ Session::has('currency') ? Session::get('currency') : '$' }} pour faire un bénéfice de " + profitPercentage + "% avec un prix d'achat de " + buyPrice + "{{ Session::has('currency') ? Session::get('currency') : '$' }}.");
}

function calculateBuyPrice() {
  var sellPrice = Number($('#sellPrice2').val());
  var profitPercentage = Number($('#profitPercentage2').val());
  var result = sellPrice + sellPrice * profitPercentage / 100;
  $("#buyPriceResult").html("Vous devez acheter à " + result + "{{ Session::has('currency') ? Session::get('currency') : '$' }} pour faire un bénéfice de " + profitPercentage + "% avec un prix de vente de " + sellPrice + "{{ Session::has('currency') ? Session::get('currency') : '$' }}.");
}

function calculateProfitPercentage() {
  var buyPrice = Number($('#buyPrice3').val());
  var sellPrice = Number($('#sellPrice3').val());
  var result = ((sellPrice - buyPrice) / buyPrice) * 100;
  $("#profitPercentageResult").html("Vous ferez un bénéfice de " + result + "% avec un prix d'achat de " + buyPrice + "{{ Session::has('currency') ? Session::get('currency') : '$' }} et un prix de vente de " + sellPrice + "{{ Session::has('currency') ? Session::get('currency') : '$' }}.");
}
</script>
@endsection
