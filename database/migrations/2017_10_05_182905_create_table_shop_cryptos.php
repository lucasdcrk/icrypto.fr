<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableShopCryptos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('shop_cryptos', function (Blueprint $table) {
          $table->increments('id');
          $table->string('name')->unique();
          $table->string('slug')->unique();
          $table->decimal('send_fees', 3, 2)->nullable();
          $table->decimal('buy_fees', 3, 2)->nullable();
          $table->decimal('sell_fees', 3, 2)->nullable();
          $table->integer('buy')->default(0);
          $table->integer('sell')->default(0);
          $table->timestamps();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('shop_cryptos');
    }
}
