#!/bin/bash
echo "Going into web folder ..."
cd /var/www/icrypto.fr
echo "Maintenance mode ..."
php artisan down
echo "Pulling from Gitlab ..."
git pull
echo "Updating dependencies ..."
composer update
echo "Updating database ..."
php artisan migrate --force
echo "END Maintenance ..."
php artisan up
