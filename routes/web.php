<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
  return view('welcome');
});

Route::get('/donate', function () {
  return view('donate');
});

Route::get('/mine', function () {
  return view('users.mine');
});

Route::get('/cs', function () {
  return view('comingsoon');
});

Route::get('/tools/calculator', function () {
  return view('tools.calculator');
});

Route::get('/tools/converter', function () {
  return view('tools.converter');
});

Route::get('/tools/converter/old', function () {
  return view('tools.oldconverter');
});

Route::get('/forum', 'ForumController@index');
Route::get('/forum/t/{slug}', 'ForumController@showThread');
Route::post('/forum/t/{slug}/reply', 'ForumController@postReply');
Route::get('/forum/c', 'ForumController@listCategories');
Route::get('/forum/c/{slug}', 'ForumController@showCategory');

Route::get('/contact', 'ContactController@index');
Route::post('/contact', 'ContactController@sendContact');

Auth::routes();

Route::get('/account', 'HomeController@index')->name('home');
Route::get('/account/settings', 'HomeController@indexSettings')->middleware('auth');
Route::get('/account/delete', 'ContactController@index')->middleware('auth');
Route::post('/account/settings/email', 'HomeController@updateEmail')->middleware('auth');
Route::post('/account/settings/password', 'HomeController@updatePassword')->middleware('auth');
Route::post('/account/settings/coinhive', 'HomeController@updateCoinhive')->middleware('auth');

Route::get('/faucet', 'FaucetController@index')->middleware('auth');
Route::post('/faucet', 'FaucetController@getFaucet')->middleware('auth');

Route::get('/games/coinflip', 'GameController@coinflip')->middleware('auth');
Route::post('/games/coinflip', 'GameController@coinflipPlay')->middleware('auth');
Route::get('/games/gambling', 'GameController@gambling')->middleware('auth');
Route::post('/games/gambling', 'GameController@gamblingPlay')->middleware('auth');

Route::get('/shop', 'ShopController@index')->middleware('auth');
Route::post('/shop/buy/crypto', 'ShopController@buyCrypto')->middleware('auth');
Route::post('/shop/sell/crypto', 'ShopController@sellCrypto')->middleware('auth');

Route::get('/admin/dashboard', 'AdminController@index')->middleware('auth', 'admin');
Route::get('/admin/updateUsersBalance', 'AdminController@updateUsersBalance');
Route::get('/admin/users', 'AdminController@listUsers')->middleware('auth', 'admin');
Route::get('/admin/users/show/{id}', 'AdminController@showUser')->middleware('auth', 'admin');
Route::get('/admin/users/edit/{id}', 'AdminController@editUser')->middleware('auth', 'admin');
Route::post('/admin/users/edit/{id}', 'AdminController@updateUser')->middleware('auth', 'admin');
Route::get('/admin/shop/cryptos', 'AdminController@shop_ListCryptos')->middleware('auth', 'admin');
Route::get('/admin/shop/cryptos/show/{id}', 'AdminController@shop_ShowCrypto')->middleware('auth', 'admin');
Route::get('/admin/shop/cryptos/edit/{id}', 'AdminController@shop_EditCrypto')->middleware('auth', 'admin');
Route::post('/admin/shop/cryptos/edit/{id}', 'AdminController@shop_UpdateCrypto')->middleware('auth', 'admin');
