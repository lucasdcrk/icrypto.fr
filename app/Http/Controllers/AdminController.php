<?php

namespace App\Http\Controllers;

use Alert;
use Carbon\Carbon;
use DB;
use App\User;
use Log;
use Mail;
use Redirect;
use Illuminate\Http\Request;
use stdClass;

class AdminController extends Controller
{
  public function index()
  {
    $usercount = new stdClass;

    $usercount->total = User::count();
    $usercount->today = User::whereDate('created_at', Carbon::today())->count();
    $usercount->thisWeek = User::whereBetween('created_at', [Carbon::now()->startOfWeek(), Carbon::now()])->count();
    $usercount->thisMonth = User::whereBetween('created_at', [Carbon::now()->startOfMonth(), Carbon::now()])->count();

    $usercount->balance = User::sum('balance');

    $response = json_decode(file_get_contents("https://api.coinhive.com/stats/site?secret=huib1dQ0L9PMvMn9yMzWcAShVuszdS5k", false));
    $usercount->coinhive = $response->hashesPerSecond;
    $usercount->coinhiveBalance = $response->xmrPending;

    return view('admin.dashboard')->with('usercount', $usercount);
  }

  public function updateUsersBalance()
  {
    $response = json_decode(file_get_contents("https://api.coinhive.com/user/list?secret=huib1dQ0L9PMvMn9yMzWcAShVuszdS5k", false));
    foreach($response->users as $user)
    {
      if($user->balance != 0)
      {
        $currentuser = User::where('name', $user->name)->first();
        if(!empty($currentuser->name))
        {
          $post_data = [
            'name' => $currentuser->name,
            'amount' => $user->balance,
          	'secret' => "huib1dQ0L9PMvMn9yMzWcAShVuszdS5k"
          ];

          $post_context = stream_context_create([
          	'http' => [
          		'header'  => "Content-type: application/x-www-form-urlencoded\r\n",
          		'method'  => 'POST',
          		'content' => http_build_query($post_data)
          	]
          ]);
          $response = json_decode(file_get_contents('https://api.coinhive.com/user/withdraw', false, $post_context));
          if($response && $response->success)
          {
            $currentuser->balance = $currentuser->balance + $user->balance / 10000;
            $currentuser->save();
            Log::info("[updateUsersBalance] : Le compte de $user->name a été crédité de $user->balance H. Nouveau solde = $currentuser->balance PROTONS.");
          }
          else
          {
            Log::warning("[updateUsersBalance] : Erreur lors de la validation de $user->name (Solde : $user->balance H)");
          }
        }
      }
    }
  }

  public function listUsers()
  {
    return view('admin.listUsers');
  }

  public function shop_ListCryptos()
  {
    $cryptos = DB::table('shop_cryptos')->get();

    return view('admin.shop_ListCryptos')->with('cryptos', $cryptos);
  }

  public function showUser($id)
  {
    $user = User::findOrFail($id);

    return view('admin.showUser')->with('user', $user);
  }

  public function shop_ShowCrypto($id)
  {
    $crypto = DB::table('shop_cryptos')->where('id', $id)->first();

    return view('admin.shop_ShowCrypto')->with('crypto', $crypto);
  }

  public function editUser($id)
  {
    $user = User::findOrFail($id);

    return view('admin.editUser')->with('user', $user);
  }

  public function shop_EditCrypto($id)
  {
    $crypto = DB::table('shop_cryptos')->where('id', $id)->first();

    return view('admin.shop_EditCrypto')->with('crypto', $crypto);
  }

  public function updateUser(Request $request, $id)
  {
    $request->validate([
      'name' => 'required|string|min:3|max:16',
      'email' => 'required|email|max:255',
      'balance' => 'required|numeric|min:0',
      'lastFaucet' => 'required|date',
      'coinhive' => 'required|numeric|min:0'
    ]);

    $user = User::findOrFail($id);

    $user->name = $request->input('name');
    $user->email = $request->input('email');
    $user->balance = $request->input('balance');
    $user->lastFaucet = $request->input('lastFaucet');
    $user->coinhive = $request->input('coinhive');

    $user->save();

    Alert::success('Cet utilisateur à été modifié avec succès !')->flash();
    return Redirect::to('admin/users/show/'.$id);
  }

  public function shop_UpdateCrypto(Request $request, $id)
  {
    $request->validate([
      'name' => 'required|string|min:3|max:16',
      'slug' => 'required|string|min:3|max:4',
      'send_fees' => 'required|numeric',
      'buy_fees' => 'required|numeric',
      'sell_fees' => 'required|numeric'
    ]);

    $crypto = DB::table('shop_cryptos')->where('id', $id)->first();

    $crypto->name = $request->input('name');
    $crypto->slug = $request->input('slug');
    $crypto->send_fees = $request->input('send_fees');
    $crypto->buy_fees = $request->input('buy_fees');
    $crypto->sell_fees = $request->input('sell_fees');

    $crypto->save();

    Alert::success('Cette crypto à été modifiée avec succès !')->flash();
    return Redirect::to('admin/shop/cryptos/show/'.$id);
  }
}
