<?php

namespace App\Http\Controllers;

use Auth;
use Alert;
use Mail;
use Redirect;
use Illuminate\Http\Request;

class ContactController extends Controller
{
  public function index()
  {
    return view('contact');
  }

  public function sendContact(Request $request)
  {
    if(Auth::check())
    {
      $request->validate([
        'message' => 'required|string|min:30|max:1000',
      ]);

      $username = Auth::user()->name;
      $email = Auth::user()->email;
    }
    else
    {
      $request->validate([
        'username' => 'required|string|min:3|max:32',
        'email' => 'required|email|max:255',
        'message' => 'required|string|min:30|max:1000',
      ]);

      $username = $request->input('username');
      $email = $request->input('email');
    }

    $messageContact = $request->input('message');

    Mail::send('emails.contact',
    array(
      'username' => $username,
      'email' => $email,
      'messageContact' => $messageContact
    ),
    function($message)
    {
      $message->to('contact.icrypto@gmail.com', 'iCrypto Admin')->subject('[CONTACT] Nouveau message reçu.');
    });

    Alert::success('Votre message a bien été envoyé.')->flash();
    return Redirect::to('contact');
  }
}
