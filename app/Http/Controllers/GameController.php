<?php

namespace App\Http\Controllers;

use Auth;
use Alert;
use Log;
use Illuminate\Http\Request;
use stdClass;

class GameController extends Controller
{
  public function coinflip()
  {
    return view('users.coinflip');
  }

  public function coinflipPlay(Request $request)
  {
    $request->validate([
      'amount' => 'required|numeric|min:0.01'
    ]);

    if(Auth::user()->balance < $request->input('amount'))
    {
      Alert::error('Vous ne disposez pas des fonds suffisants.');
      return view('users.coinflip');
      exit;
    }

    $coinflip = new stdClass;

    $coinflip->rand = rand(0, 100);
    $coinflip->amount = $request->input('amount');

    $user = Auth::user();
    $balance = $user->balance;

    if($coinflip->rand < 47)
    {
      $user->balance = $balance + $coinflip->amount;
      $user->save();

      $coinflip->state = '1';

      Alert::info('Bravo ! Vous avez gagné '.$coinflip->amount.' PROTONS.');
      Log::info("[Coinflip] $user->name A gagné $coinflip->amount PROTONS, nouveau solde : $user->balance PROTONS.");

      return view('users.coinflip')->with('coinflip', $coinflip);
    }
    else
    {
      $user->balance = $balance - $coinflip->amount;
      $user->save();

      $coinflip->state = '0';

      Alert::info('Dommage ! Vous avez perdu '.$coinflip->amount.' PROTONS.');
      Log::info("[Coinflip] $user->name A perdu $coinflip->amount PROTONS, nouveau solde : $user->balance PROTONS.");

      return view('users.coinflip')->with('coinflip', $coinflip);
    }
  }

  public function gambling()
  {
    return view('users.gambling');
  }

  public function gamblingPlay(Request $request)
  {
    $request->validate([
      'amount' => 'required|numeric|min:0.01',
      'payout' => 'required|numeric|min:1|max:100'
    ]);

    if(Auth::user()->balance < $request->input('amount'))
    {
      Alert::error('Vous ne disposez pas des fonds suffisants.');
      return view('users.gambling');
      exit;
    }

    $gambling = new stdClass;

    $gambling->rand = rand(0, 100);
    $gambling->amount = $request->input('amount');

    $user = Auth::user();
    $balance = $user->balance;

    if($gambling->rand < 47)
    {
      $user->balance = $balance + $gambling->amount;
      $user->save();

      $gambling->state = '1';

      Alert::info('Bravo ! Vous avez gagné '.$gambling->amount.' PROTONS.');
      Log::info("[Gambling] $user->name A gagné $gambling->amount PROTONS, nouveau solde : $user->balance PROTONS.");

      return view('users.gambling')->with('gambling', $gambling);
    }
    else
    {
      $user->balance = $balance - $gambling->amount;
      $user->save();

      $gambling->state = '0';

      Alert::info('Dommage ! Vous avez perdu '.$gambling->amount.' PROTONS.');
      Log::info("[Gambling] $user->name A perdu $gambling->amount PROTONS, nouveau solde : $user->balance PROTONS.");

      return view('users.gambling')->with('gambling', $gambling);
    }
  }
}
