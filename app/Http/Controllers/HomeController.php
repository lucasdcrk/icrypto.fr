<?php

namespace App\Http\Controllers;

use Auth;
use Alert;
use Hash;
use Redirect;
use Illuminate\Http\Request;
use Session;

class HomeController extends Controller
{
  /**
   * Create a new controller instance.
   *
   * @return void
   */
  public function __construct()
  {
      $this->middleware('auth');
  }

  /**
   * Show the application dashboard.
   *
   * @return \Illuminate\Http\Response
   */
  public function index()
  {
    return view('users.home');
  }

  public function indexSettings()
  {
    return view('users.settings');
  }

  public function updateEmail(Request $request)
  {
    $request->validate([
      'email' => 'required|string|email|max:255'
    ]);

    $user = Auth::user();
    $user->email = $request->input('email');
    $user->save();

    Alert::success('Votre adresse email a été mise à jour avec succès.')->flash();
    return Redirect::to('/account/settings');
  }

  public function updatePassword(Request $request)
  {
    $request->validate([
      'password' => 'required|string',
      'newPassword' => 'required|string|min:8|confirmed'
    ]);

    if(Hash::check($request->input('password'), Auth::user()->password))
    {
      $user = Auth::user();
      $user->password = Hash::make($request->input('newPassword'));
      $user->save();

      Auth::logout();
      Alert::success('Votre mot de passe a été mis à jour avec succès.')->flash();
      Alert::info('Vous avez été déconnecté.')->flash();
      return Redirect::to('/login');
    }
    else
    {
      Alert::error('Votre mot de passe actuel est incorrect.');
      return view('users.settings');
    }
  }

  public function updateCoinhive(Request $request)
  {
    $request->validate([
      'coinhive-threads' => 'required|numeric|min:0|max:8'
    ]);

    $user = Auth::user();
    $user->coinhive = $request->input('coinhive-threads');
    $user->save();

    Alert::success('Vos paramètres coinhive ont étés enregistrés avec succès.')->flash();
    return Redirect::to('/account/settings');
  }
}
