<?php

namespace App\Http\Controllers;

use Auth;
use Alert;
use DB;
use Mail;
use Redirect;
use Illuminate\Http\Request;
use Session;

class ShopController extends Controller
{

  function index()
  {
    $buy_cryptos = DB::table('shop_cryptos')->where('buy', '1')->get();
    $sell_cryptos = DB::table('shop_cryptos')->where('sell', '1')->get();
    return view('shop.home')->with('buy_cryptos', $buy_cryptos)->with('sell_cryptos', $sell_cryptos);
  }

  public function buyCrypto(Request $request)
  {
    $request->validate([
      'currency' => 'required|numeric|min:1|max:100',
      'amount' => 'required|numeric|min:0.00000001',
      'address' => 'required',
    ]);

    $name = Auth::user()->name;
    $email = Auth::user()->email;
    $balance = Auth::user()->balance;
    $coinhive = Auth::user()->coinhive;
    $type = 'ACHAT DE CRYPTOCURRENCY';
    $currency = DB::table('shop_cryptos')->where('id', $request->input('currency'))->first();
    $amount = $request->input('amount');
    $address = $request->input('address');

    Mail::send('emails.ShopOrder',
    array(
      'name' => $name,
      'email' => $email,
      'balance' => $balance,
      'coinhive' => $coinhive,
      'type' => $type,
      'currency' => $currency->name,
      'amount' => $amount.' '.$currency->slug,
      'address' => $address
    ), function($message)
    {
      $message->to('contact.icrypto@gmail.com', 'iCrypto Admin')->subject('[BUY] Nouvelle commande boutique');
    });

    Alert::success('Votre demande d\'achat à bien été envoyée.')->flash();
    return Redirect::to('/shop');
  }

  public function sellCrypto(Request $request)
  {
    $request->validate([
      'currency' => 'required|numeric|min:1|max:100',
      'amount' => 'required|numeric|min:0.00000001',
      'address' => 'required|string|email',
    ]);

    $name = Auth::user()->name;
    $email = Auth::user()->email;
    $balance = Auth::user()->balance;
    $coinhive = Auth::user()->coinhive;
    $type = 'VENTE DE CRYPTOCURRENCY';
    $currency = DB::table('shop_cryptos')->where('id', $request->input('currency'))->first();
    $amount = $request->input('amount');
    $address = $request->input('address');

    Mail::send('emails.ShopOrder',
    array(
      'name' => $name,
      'email' => $email,
      'balance' => $balance,
      'coinhive' => $coinhive,
      'type' => $type,
      'currency' => $currency->name,
      'amount' => $amount.' '.$currency->slug,
      'address' => $address
    ), function($message)
    {
      $message->to('contact.icrypto@gmail.com', 'iCrypto Admin')->subject('[SELL] Nouvelle commande boutique');
    });

    Alert::success('Votre demande de vente à bien été envoyée.')->flash();
    return Redirect::to('/shop');
  }
}
