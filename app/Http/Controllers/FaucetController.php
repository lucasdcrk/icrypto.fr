<?php

namespace App\Http\Controllers;

use Auth;
use Alert;
use Carbon\Carbon;
use Log;
use Redirect;
use Illuminate\Http\Request;

class FaucetController extends Controller
{
  public function index()
  {
    $user = Auth::user();
    $now = Carbon::now();
    $lastFaucet = Carbon::parse($user->lastFaucet);
    $nextFaucet = Carbon::parse($lastFaucet)->addHour();

    return view('users.faucet')->with('lastFaucet', $lastFaucet)->with('nextFaucet', $nextFaucet)->with('now', $now);
  }

  public function getFaucet()
  {
    $user = Auth::user();
    $now = Carbon::now();
    $lastFaucet = Carbon::parse($user->lastFaucet);

    if($now->diffInMinutes($lastFaucet) > 1)
    {
      $user->lastFaucet = $now;
      $user->balance = $user->balance + 1;
      $user->save();
      Alert::success('Votre compte a bien été crédité de 1 PROTON.')->flash();
      Log::info("[Faucet] $user->name A récupéré son faucet, nouveau solde : $user->balance PROTONS.");
    }
    else
    {
      $nextFaucet = $lastFaucet->addHour();
      Alert::error('Vous avez déjà obtenu votre faucet il y moins d\'une heure.')->flash();
    }

    return Redirect::to('faucet');
  }
}
