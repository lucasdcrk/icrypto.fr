<?php

namespace App\Http\Middleware;

use Auth;
use Alert;
use Closure;
use Redirect;

class CheckIfAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
      if(Auth::user()->isAdmin != 1)
      {
        Alert::error('Vous n\'êtes pas autorisé à accèder à cette page.')->flash();
        return Redirect::to('/');
      }

      return $next($request);
    }
}
