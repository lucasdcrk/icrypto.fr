<?php

namespace App\Http\Middleware;

use Alert;
use Closure;
use Illuminate\Support\Facades\Auth;

class RedirectIfAuthenticated
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        if (Auth::guard($guard)->check()) {
          Alert::info('Vous avez été automatiquement reconnecté.')->flash();
          return redirect('/account');
        }

        return $next($request);
    }
}
